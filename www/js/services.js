angular.module('danvers.services', [])

.factory('Events', function($http, $q){

  var service = {
    getCalendars: getCalendars,
    getEvents: getEvents
  }

  var apiKey = 'AIzaSyCrRN8Vbx2xucYmLStrLzh-r9RfMAKxcUU',
      urlBase = 'https://www.googleapis.com/calendar/v3/calendars/',
      urlSuffix = '/events',
      params = {
        maxResults: '20',
        timeMin: moment().toISOString(),
        key: apiKey,
        orderBy: 'startTime',
        singleEvents: 'true'
      };

  var calendars = [
    {
      name: 'DHS Academics',
      calId: 'danvers.org_p5lernnffu4mr690tc2jnp2cc8@group.calendar.google.com',
    },
    {
      name: 'Great Oak Elementary',
      calId: 'danvers.org_mcj0q7vcouuo2vd7d418so6bjg@group.calendar.google.com',
    },
    {
      name: 'Highlands Elementary',
      calId: 'danvers.org_ce2vufbm3frh53mpsqf26nh71k@group.calendar.google.com',
    },
    {
      name: 'Holten Richmond Middle School',
      calId: 'danvers.org_1f516lnf8cbmes3246aqqb63cc@group.calendar.google.com'
    },
    {
      name: 'Riverside Elementary',
      calId: 'danvers.org_61sdqe8rgm3igq6r8gs9vi2jio@group.calendar.google.com'
    },
    {
      name: 'Smith Elementary',
      calId: 'danvers.org_0mmucukqik9gp1d6ivcrbhasng@group.calendar.google.com'
    },
    {
      name: 'Thorpe Elementary',
      calId: 'danvers.org_lm6oslmjvh31vv8uuggreob1jc@group.calendar.google.com'
    },
    {
      name: 'District Calendar',
      calId: 'danvers.org_bea0med8ao4cp93dp3dmd6o3bs@group.calendar.google.com'
    }
  ];

  function getEvents() {
    var promises = [];

    calendars.forEach(function(el, index, array) {
      var url = urlBase + el.calId + urlSuffix;
      promises.push(
        $http({
          method: 'GET',
          url: url,
          params: params,
          cache:true,
          timeout: 5000
        })
      );
    });

    return $q.all(promises);
  }

  function getCalendars() {
    return calendars;
  }

  return service;

})

.factory('Staff', function($http){

  return {
    getStaff: function() {
      return $http({
        method: 'GET',
        url: "http://danverspublicschools.org/st-api/staff/20",
        cache: true
      });
    },
  };
})

.factory('Schools', function() {
  var schools = [
    {
      name: 'Danvers High School',
      principal: {
        name: 'Sue Ambrozavitch',
        email: 'ambrozav@danvers.org'
      },
      administrators: [
        {
          name: 'Mark Strout',
          email: 'strout@danvers.org',
          position: 'Assistant Principal'
        },
        {
          name: 'Mark DeCiccio',
          email: 'deciccio@danvers.org',
          position: 'Assistant Principal'
        },
      ],
      location: {
        address: '60 Cabot Road, Danvers, MA 01923',
        lat: '42.583049',
        lng: '-70.931499'
      },
      image: 'img/schools/high.jpg',
      handbook: 'http://danverspublicschools.org/wp-content/uploads/2014/01/DHS-15-16-Student-Handbook-Final-use.pdf',
      phone: '(978) 777-8925',
      fax: '(978) 777-8931',
      email: 'info@danvers.org',
      website: 'http://danverspublicschools.org/dhs/'
    },
    {
      name: 'Holten Richmond Middle School',
      principal: {
        name: 'Adam Federico',
        email: 'federico@danvers.org'
      },
      administrators: [
        {
          name: 'Deborah Arno',
          email: 'arno@danvers.org',
          position: 'Assistant Principal'
        },
        {
          name: 'Julie Macdonald',
          email: 'juliemacdonald@danvers.org',
          position: 'Assistant Principal'
        },
        {
          name: 'Ellyn Feerick',
          email: 'ellynfeerick@danvers.org',
          position: 'Interim Curriculum Director'
        }
      ],
      location: {
        address: '55 Conant Street, Danvers, MA 01923',
        lat: '42.567589',
        lng: '-70.929439'
      },
      image: 'img/schools/middle.jpg',
      handbook: 'http://danverspublicschools.org/wp-content/uploads/2014/01/HRMS-Parent_Student-Handbk-2015-2016-rev1.pdf',
      phone: '(978) 774-8590',
      fax: '(978) 762-8686',
      email: 'info@danvers.org',
      website: 'http://danverspublicschools.org/holten-richmond/'
    },
    {
      name: 'Great Oak Elementary',
      principal: {
        name: 'Sharon Burrill',
        email: 'burrill@danvers.org'
      },
      administrators: [
        {
          name: 'Jennifer Koulopoulos',
          email: 'jenniferkoulopoulos@danvers.org',
          position: 'Assistant Principal'
        },
      ],
      location: {
        address: '76 Pickering Street, Danvers, MA 01923',
        lat: '42.569863',
        lng: '-70.948777',
      },
      image: 'img/schools/great-oak.jpg',
      handbook: 'http://danverspublicschools.org/wp-content/uploads/2014/01/elementary-handbook-revised-8.10.15.pdf',
      phone: '(978) 774-2533',
      fax: '(978) 777-1471',
      email: 'info@danvers.org',
      website: 'http://danverspublicschools.org/great-oak/'
    },
    {
      name: 'Highlands Elementary',
      principal: {
        name: 'Elizabeth Matthews',
        email: 'matthews@danvers.org'
      },
      administrators: [
        {
          name: 'Edju Gasinowski',
          email: 'edjugasinowski@danvers.org',
          position: 'Assistant to the Principal'
        },
      ],
      location: {
        address: '190 Hobart Street, Danvers, MA 01923',
        lat: '42.564421',
        lng: '-70.959319',
      },
      image: 'img/schools/highlands.jpg',
      handbook: 'http://danverspublicschools.org/wp-content/uploads/2014/01/elementary-handbook-revised-8.10.15.pdf',
      phone: '(978) 774-5011',
      fax: '(978) 777-5821',
      email: 'info@danvers.org',
      website: 'http://danverspublicschools.org/highlands/'
    },
    {
      name: 'Riverside Elementary',
      principal: {
        name: 'Violetta Powers',
        email: 'violettapowers@danvers.org'
      },
      administrators: [
        {
          name: 'Sarah Fitzgerald',
          email: 'sarahfitzgerald@danvers.org',
          position: 'Assistant to the Principal'
        },
      ],
      location: {
        address: '95 Liberty Street, Danvers, MA 01923',
        lat: '42.560231',
        lng: '-70.923801',
      },
      image: 'img/schools/riverside.jpg',
      handbook: 'http://danverspublicschools.org/wp-content/uploads/2014/01/elementary-handbook-revised-8.10.15.pdf',
      phone: '(978) 774-5010',
      fax: '(978) 774-7850',
      email: 'info@danvers.org',
      website: 'http://danverspublicschools.org/riverside/'
    },
    {
      name: 'Ivan G. Smith Elementary',
      principal: {
        name: 'Tracey Mara',
        email: 'mara@danvers.org'
      },
      administrators: [
        {
          name: 'Laurie Lawler',
          email: 'laurielawler@danvers.org',
          position: 'Assistant to the Principal'
        },
      ],
      location: {
        address: '15 Lobao Drive, Danvers, MA 01923',
        lat: '42.587639',
        lng: '-70.953912',
      },
      image: 'img/schools/smith.jpg',
      handbook: 'http://danverspublicschools.org/wp-content/uploads/2014/01/elementary-handbook-revised-8.10.15.pdf',
      phone: '(978) 774-1350',
      fax: '(978) 774-1351',
      email: 'info@danvers.org',
      website: 'http://danverspublicschools.org/smith/'
    },
    {
      name: 'Thorpe Elementary',
      principal: {
        name: 'Rita Ward',
        email: 'ritaward@danvers.org'
      },
      administrators: [
        {
          name: 'Christine Proctor',
          email: 'christineproctor@danvers.org',
          position: 'Assistant to the Principal'
        },
        {
          name: 'Ann Gagnon',
          email: 'anngagnon@danvers.org',
          position: 'School Secretary'
        }
      ],
      location: {
        address: 'Avon Road, Danvers, MA 01923',
        lat: '42.572477',
        lng: '-70.920523',
      },
      image: 'img/schools/thorpe.jpg',
      handbook: 'http://danverspublicschools.org/wp-content/uploads/2014/01/elementary-handbook-revised-8.10.15.pdf',
      phone: '(978) 774-6946',
      fax: '(978) 774-4417',
      email: 'info@danvers.org',
      website: 'http://danverspublicschools.org/thorpe/',
    }
  ];
  return {
    all: function(){
      return schools;
    },
    get: function(index) {
      return schools[index];
    }
  };
})

.factory('Facebook', function($http) {

  var accessToken = '1081167901899265|lJb2whvfZKd6VXTMNBOREpMC7Qs',
      pageId = '281720751861771',
      jsonUrl = 'https://graph.facebook.com/' + pageId + '/feed?access_token=' + accessToken;

      return {
        getPosts: function() {
          return $http({
            method: 'GET',
            url: jsonUrl,
            params: {
              limit: 5,
              offset: this.getOffset()
            }
          });
        },
        setOffset: function(page) {
          this.offset = page * 5;
        },
        getOffset: function() {
          return this.offset;
        }
      }
})

.factory('Twitter', function($http) {
  var clientId = 'Dcz8N7mziGwxbkv3iy9tOZGEz',
      clientSecret = 'uf2hBbssls1TafKsqozaRwy37ZTuvcQ347EILa9tGUcQhUgX41',
      bearerTokenCredentials = clientId + ':' + clientSecret,
      encodedCredentials = btoa(bearerTokenCredentials);

  return {
    getTweets: function() {
      return $http({
        method: 'GET',
        url: 'https://api.twitter.com/1.1/statuses/user_timeline.json',
        headers: {
          'Authorization': 'Bearer AAAAAAAAAAAAAAAAAAAAAGnsiAAAAAAAo%2BMwyRI%2BG8mnntZS5RMSMQ9YdUo%3DtSLgRdVTSbnrvVS8KJRRwj6D4hRl6xGZnmlUw7SbKNiBZrHxNn'
        },
        params: {
          'screen_name': 'Tweet_DPS',
          'count': 20,
          'max_id': this.getMaxId()
        }
      });
    },
    setMaxId: function(maxId) {
      this.maxId = maxId;
    },
    getMaxId: function() {
      return this.maxId;
    },
    getBearerToken: function() {
      return $http({
        method: 'POST',
        url: 'https://api.twitter.com/oauth2/token',
        headers: {
          'Authorization': 'Basic ' + encodedCredentials,
          'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
        },
        data: 'grant_type=client_credentials'
      });
    }
  }
})

.factory('Posts', function($http) {

  var service = {
      posts: [],
      all: getDistrictPosts,
      bySchool: getPostsbySchool,
      bySchoolandId: getPostbySchool
  };

  return service;

  function getDistrictPosts() {
    return $http.get(
      'http://danverspublicschools.org/wp-json/wp/v2/posts'
    )
    .then(function(data) {
        service.posts = data;
    });
  }

  function getPostsbySchool(school){
    var url = school.website + 'wp-json/wp/v2/posts';

    return $http({
      url: url,
      method: 'GET',
      cache: true
    })
    .success(function(data) {
        service.posts = data;
    });
  }

  function getPostbySchool(school, postId){
    var url = school.website + 'wp-json/wp/v2/posts/' + postId;
    return $http({
      url: url,
      method: 'GET',
      cache: true
    })
    .success(function(data) {
        service.posts = data;
    });
  }
})

.directive('ionSearch', function() {
    return {
        restrict: 'E',
        replace: true,
        scope: {
            getData: '&source',
            model: '=?',
            search: '=?filter'
        },
        link: function(scope, element, attrs) {
            attrs.minLength = attrs.minLength || 0;
            scope.placeholder = attrs.placeholder || '';
            scope.search = {value: ''};

            if (attrs.class)
                element.addClass(attrs.class);

            if (attrs.source) {
                scope.$watch('search.value', function (newValue, oldValue) {
                    if (newValue.length > attrs.minLength) {
                        scope.getData({str: newValue}).then(function (results) {
                            scope.model = results;
                        });
                    } else {
                        scope.model = [];
                    }
                });
            }

            scope.clearSearch = function() {
                scope.search.value = '';
            };
        },
        template: '<div class="item-input-wrapper">' +
                    '<i class="icon ion-android-search"></i>' +
                    '<input type="search" placeholder="{{placeholder}}" ng-model="search.value">' +
                    '<i ng-if="search.value.length > 0" ng-click="clearSearch()" class="icon ion-close"></i>' +
                  '</div>'
    };
})
.factory('$localstorage', ['$window', function($window) {
  return {
    set: function(key, value) {
      $window.localStorage[key] = value;
    },
    get: function(key, defaultValue) {
      return $window.localStorage[key] || defaultValue;
    },
    setObject: function(key, value) {
      $window.localStorage[key] = JSON.stringify(value);
    },
    getObject: function(key) {
      return JSON.parse($window.localStorage[key] || false);
    }
  }
}])
.filter('html',function($sce){
    return function(input){
        return $sce.trustAsHtml(input);
    }
})
.filter('linkTwitterHandles', function(){
  return function (input) {
    var out,
        regex   = /(^|[^@\w])@(\w{1,15})\b/g,
        replace = '$1<a href="http://twitter.com/$2">@$2</a>';

    out = input.replace(regex, replace);
    return out;
  };
});
