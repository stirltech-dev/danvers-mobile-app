'use strict';

angular.module('danvers', [
  'ionic',
  'danvers.controllers',
  'danvers.services',
  'ngCordova',
  'uiGmapgoogle-maps',
  'angularMoment',
  'ngIOS9UIWebViewPatch',
  'angular.filter',
  'jett.ionic.filter.bar'
])

.run(function($ionicPlatform, $cordovaNetwork, $cordovaDialogs, $cordovaStatusbar) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(false);
      cordova.plugins.Keyboard.disableScroll(true);
    }

    //Let them know they have no internet connection
    if (window.cordova && $cordovaNetwork.isOffline()) {
      console.log('no network');
      $cordovaDialogs.alert('Some functions of this app may not work as intended.', 'No Internet Connection');
    }

    if (window.cordova && cordova.InAppBrowser) {
      //We want this so we can use window.open and have it use the InAppBrowser
      window.open = cordova.InAppBrowser.open;
    }

    //Make the staus bar blue
    if (window.StatusBar) {
      $cordovaStatusbar.style(1);
    }

    if (window.cordova) {
      //Get the pushwoosh device token
      if (ionic.Platform.isIOS()) {
        initPushwooshiOS();
      }
      else if (ionic.Platform.isAndroid()) {
        initPushwooshAndroid();
      }
    }
  });

})

.config(function($stateProvider, $urlRouterProvider, $ionicFilterBarConfigProvider) {

  $ionicFilterBarConfigProvider.theme('positive');
  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/tab/dash');
})

.directive('launchLinks', function($document, $parse){
  var linkFn = function($scope, $element, $attributes) {
    $element.on(
      "click",
      function( e ){
        e = e ||  window.event;
        var element = e.target || e.srcElement;

        if (element.tagName == 'A') {
          e.preventDefault();
          window.open(element.href, '_blank', 'EnableViewPortScale=yes,location=no');
          return false; // prevent default action and stop event propagation
        }
      }
    );
  }
   return {
      restrict: 'A', //attribute only
      link: linkFn
   };
});
