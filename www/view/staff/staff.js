'use strict';

angular.module('danvers')

.config(['$stateProvider', function($stateProvider){
  $stateProvider.state('tab.staff', {
    url: '/staff',
    views: {
      'tab-more': {
        templateUrl: 'view/staff/staff.html',
        controller: 'StaffCtrl'
      }
    }
  })
}])

.controller('StaffCtrl', [
  '$scope',
  'Staff',
  '$cordovaEmailComposer',
  '$ionicLoading',
  '$ionicFilterBar',
  function($scope, Staff, $cordovaEmailComposer, $ionicLoading, $ionicFilterBar) {
    $ionicLoading.show({
        template: 'Loading...'
      });

    Staff.getStaff()
      .then(function(data){
        $scope.staff = data.data.staff;
        $ionicLoading.hide();
      }, function(err) {
        $scope.error = 'There was an error loading the staff directory. Please check your network connection.';
        $ionicLoading.hide();
      });

    $scope.sendEmail = function(emailAddress) {
      document.addEventListener('deviceready', function () {
        var email = {
          to: emailAddress,
        };
        cordova.plugins.email.open(email);
      }, false);
    }

    $scope.showFilterBar = function () {
      filterBarInstance = $ionicFilterBar.show({
        items: $scope.staff,
        update: function (filteredItems, filterText) {
          $scope.staff = filteredItems;
          if (filterText) {
            console.log(filterText);
          }
        }
      });
    };

    //We check that the email is valid before showing the possibility to email
    $scope.isValidEmail = (function() {
      var regexp = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      return function(value) {
        return regexp.test(value);
      };
    })();

    $scope.emailIsAvailable = function() {
      document.addEventListener('deviceready', function () {
        cordova.plugins.email.isAvailable(
            function (isAvailable) {
                return true;
            }
        );
        return false;
      });
    }

  }
]);
