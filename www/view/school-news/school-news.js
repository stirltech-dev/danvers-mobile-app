'use strict';

angular.module('danvers')

.config(['$stateProvider', function($stateProvider){
  $stateProvider.state('tab.school-news', {
    url: '/schools/:schoolId/news',
    views: {
      'tab-schools': {
        templateUrl: 'view/school-news/school-news.html',
        controller: 'SchoolsNewsCtrl'
      }
    }
  })
}])

.controller('SchoolsNewsCtrl', [
  '$scope',
  'Schools',
  'Posts',
  '$stateParams',
  '$ionicLoading',
  function($scope, Schools, Posts, $stateParams, $ionicLoading){
    $ionicLoading.show({
        template: 'Loading...'
      });

    $scope.school = Schools.get($stateParams.schoolId);
    $scope.school.id = $stateParams.schoolId;

    Posts.bySchool($scope.school).then(function(data){
      $scope.posts = data.data;
      $ionicLoading.hide();
    }, function(err){
      $ionicLoading.hide();
      $scope.error = 'There was an error loading the posts. Please check your network connection.';
    });
  }
]);
