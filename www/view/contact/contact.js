'use strict';

angular.module('danvers')

.config(['$stateProvider', function($stateProvider){
  $stateProvider.state('tab.contact', {
    url: '/schools/:schoolId/contact',
    views: {
      'tab-schools': {
        templateUrl: 'view/contact/contact.html',
        controller: 'ContactCtrl'
      }
    }
  })
}])

.controller('ContactCtrl', [
  '$scope',
  '$http',
  '$stateParams',
  'Schools',
  function($scope, $http, $stateParams, Schools) {

    $scope.school = Schools.get($stateParams.schoolId);

    $scope.formData = {};
    $scope.errors = {};

    $scope.submit = function() {

      if (Object.getOwnPropertyNames($scope.formData).length == 0) {
        return;
      }

      //Create an object compatible with our Gravity Form
      var submissionObj = {
        input_values: {
          input_1: $scope.formData.name,
          input_2: $scope.formData.email,
          input_3: $scope.formData.phone,
          input_4: $scope.formData.message,
          input_5: $scope.school.name,
        }
      };

      $scope.processing = true; //so we can disable the button

      $http({
        method: 'POST',
        url: 'http://danverspublicschools.org/gravityformsapi/forms/12/submissions',
        data: submissionObj,
      }).then(function(data){
        var response = data.data.response;
        $scope.processing = false;
        if (!response.is_valid) {
          $scope.message = 'There were errors with your submission.';
          $scope.errors.name = data.response.validation_messages['1'];
          $scope.errors.email = data.response.validation_messages['2'];
          $scope.errors.phone = data.response.validation_messages['3'];
          $scope.errors.message = data.response.validation_messages['4'];
        }
        else {
          $scope.message = 'We have received your message and will get back to you as soon as possible.';
          $scope.formData = {};
        }
      });
    }
  }
]);
