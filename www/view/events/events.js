'use strict';

angular.module('danvers')

.config(['$stateProvider', '$urlRouterProvider', function($stateProvider){
  $stateProvider.state('tab.events', {
    url: '/events',
    views: {
      'tab-events': {
        templateUrl: 'view/events/events.html',
        controller: 'EventsCtrl'
      }
    }
  })
}])

.controller('EventsCtrl', [
  '$scope',
  'Events',
  '$ionicModal',
  '$localstorage',
  '$cordovaCalendar',
  '$ionicLoading',
  '$cordovaDialogs',
  function($scope, Events, $ionicModal, $localstorage, $cordovaCalendar, $ionicLoading, $cordovaDialogs) {

    $ionicLoading.show({
      template: 'Loading...'
    });

    $scope.today = moment();

    //Create the modal instance to select calendar
    $ionicModal.fromTemplateUrl('filter-calendars.html', {
      scope: $scope,
      animation: 'slide-in-up'
    }).then(function(modal) {
      $scope.selectCalendarModal = modal;
    });

    //Create the modal instance to select calendar
    $ionicModal.fromTemplateUrl('subscribe.html', {
      scope: $scope,
      animation: 'slide-in-up'
    }).then(function(modal) {
      $scope.subscribeModal = modal;
    });

    function sortDates(a, b) {
      return a.start - b.start;
    }

    //initialize the calendar scope
    $scope.calendars = Events.getCalendars();

    $scope.getEvents = function () {
      Events.getEvents().then(function(response){
        //initialize an events array
        var events = [];

        response.forEach(function(data){
          //get the name of the calendar and save it to a variable
          var calendar = data.data.description ? data.data.description : data.data.summary;
          //$scope.calendars.push(calendar);
          //loop through the individual items
          data.data.items.forEach(function(item, index, array){
            var event = {
              'calendar': calendar,
              'calId': item.organizer.email,
              'summary': item.summary,
              'description': item.description,
              'start': item.start.date ? moment(item.start.date) : moment(item.start.dateTime),
              'end': item.end.date ? moment(item.end.date) : moment(item.end.dateTime),
              'allDay': item.start.dateTime ? false : true,
              'location': item.location,
              'addedToCalendar': false
            }
            //we have to do this so we can have dividers grouped by date
            event.date = event.start.clone().startOf('d').format('YYYY-MM-DD');
            events.push(event);
          })
        });
        //sort the events by date
        events.sort(sortDates);
        $scope.events = events;
        setTimeout($ionicLoading.hide(), 2000); //we introduce a timeout to let the DOM figure its shit out

        //Load the previously selected filters from localstorage
        $scope.filter = $localstorage.getObject('calendars') || [];
      },
      function(error){
        $ionicLoading.hide();
        $scope.error = 'There was an error retrieving the events. Please check your network connection. Click here to try again.';
        console.log(JSON.stringify(error));
      });
    }

    $scope.clearSelection = function() {
      $scope.filter = [];
      $scope.saveAndClose();
    }

    $scope.saveAndClose = function() {
      $localstorage.setObject('calendars', $scope.filter);
      $scope.selectCalendarModal.hide();
    }


    //Logic for filtering out the events by the selected calendar

    $scope.filterByCalendar = function (event) {
      return $scope.filter[event.calId] || noFilter($scope.filter);
    }

    function noFilter(filterObj) {
        for (var key in filterObj) {
            if (filterObj[key]) {
                return false;
            }
        }
        return true;
    }

    $scope.addToCalendar = function(event) {

      var eventObj = {
        title: event.summary,
        notes: event.calendar,
        startDate: event.start,
        endDate: event.end
      }
      if (event.location) {
        eventObj.location = event.location;
      }
      $cordovaCalendar.createEventInteractively(eventObj).then(function(result) {
        console.log(result);
        event.addedToCalendar = true;
      }, function (err) {
        $cordovaDialogs.alert('There was an error adding your event to the calendar. Please try again.', 'Error Adding Event');
      });
    }

    $scope.subscribeToCalendar = function(calId) {
      window.open('https://calendar.google.com/calendar/ical/' + calId + '/public/basic.ics', '_system');
    }

    $scope.openAthleticSchedules = function() {
      window.open('http://www.northeasternma.org/g5-bin/client.cgi?G5genie=595&school_id=4', 'target=_blank', 'EnableViewPortScale=yes');
    }

    $scope.isAndroid = function() {
      document.addEventListener("deviceready", function () {
        return device.platform == 'Android';
      });
    }

    //Fetch the events
    $scope.getEvents();
  }
]);
