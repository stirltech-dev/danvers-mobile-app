'use strict';

angular.module('danvers')

.config(['$stateProvider', function($stateProvider){
  $stateProvider.state('tab.more', {
    url: '/more',
    views: {
      'tab-more': {
        templateUrl: 'view/more/more.html',
        controller: 'MoreCtrl'
      }
    }
  })
}])

.controller('MoreCtrl', [
  '$scope',
  '$cordovaInAppBrowser',
  '$cordovaDialogs',
  function($scope, $cordovaInAppBrowser, $cordovaDialogs) {
    $scope.openPowerschool = function() {
      document.addEventListener("deviceready", function () {
        $cordovaDialogs.confirm('Would you like to open/download the Powerschool app?', 'Open in App?', ['Sure', 'No Thanks'])
        .then(function(index){
          if(index == 1) { //The index starts at 1
            if (device.platform === 'iOS') {
              window.open('http://itunes.apple.com/us/app/powerschool-mobile/id973741088?mt=8', '_blank');
            }
            else if (device.platform === 'Android') {
              window.open('https://play.google.com/store/apps/details?id=com.powerschool.portal', '_blank');
            }
          }
          else{
            window.open('https://ps.danvers.mec.edu/public/home.html', '_blank', 'EnableViewPortScale=yes');
          }
        });
      });
    }

    $scope.openFoodPayments = function() {
      window.open('https://sendmoneytoschool.com/Dashboard/Login.aspx?ReturnUrl=%2f', '_blank', 'EnableViewPortScale=yes');
    }

    $scope.openLunchMenus = function() {
      window.open('http://www.schoolnutritionandfitness.com/index.php?sid=1210112058116103', '_blank', 'EnableViewPortScale=yes');
    }
  }
]);
