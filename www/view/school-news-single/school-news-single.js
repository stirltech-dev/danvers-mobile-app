'use strict';

angular.module('danvers')

.config(['$stateProvider', function($stateProvider){
  $stateProvider.state('tab.school-news-single', {
    url: '/schools/:schoolId/news/:postId',
    views: {
      'tab-schools': {
        templateUrl: 'view/school-news-single/school-news-single.html',
        controller: 'SchoolsNewsSingleCtrl'
      }
    }
  })
}])


.controller('SchoolsNewsSingleCtrl', [
  '$scope',
  'Schools',
  'Posts',
  '$stateParams',
  '$ionicLoading',
  function($scope, Schools, Posts, $stateParams, $ionicLoading){
    $ionicLoading.show({
        template: 'Loading...'
      });

    $scope.school = Schools.get($stateParams.schoolId);
    $scope.school.id = $stateParams.schoolId;
    var postId = $stateParams.postId

    Posts.bySchoolandId($scope.school, postId).then(function(data){
      $scope.post = data.data;
      console.log($scope.post);
      $ionicLoading.hide();
    });
  }
]);
