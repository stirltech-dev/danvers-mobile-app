'use strict';

angular.module('danvers')

.config(['$stateProvider', function($stateProvider){
  $stateProvider.state('tab.feedback', {
    url: '/feedback',
    views: {
      'tab-more': {
        templateUrl: 'view/feedback/feedback.html',
        controller: 'FeedbackCtrl'
      }
    }
  })
}])

.controller('FeedbackCtrl', [
  '$scope',
  '$http',
  '$cordovaGeolocation',
  '$cordovaDialogs',
  '$cordovaCamera',
  '$cordovaActionSheet',
  function($scope, $http, $cordovaGeolocation, $cordovaDialogs, $cordovaCamera, $cordovaActionSheet) {

    $scope.images = [];

    var posOptions = {timeout: 10000, enableHighAccuracy: false};

    // var actionSheetOptions = {
    //   buttonLabels: ['Take Photo', 'Choose Photo'],
    //   addCancelButtonWithLabel: 'Cancel',
    //   androidEnableCancelButton : true,
    //   winphoneEnableCancelButton : true,
    // };
    //
    // var cameraOptions = {
    //   quality: 50,
    //   destinationType: Camera.DestinationType.FILE_URI,
    //   allowEdit: true,
    //   encodingType: Camera.EncodingType.JPEG,
    //   popoverOptions: CameraPopoverOptions,
    //   saveToPhotoAlbum: false,
    //   correctOrientation:true
    // };

    $scope.getLocation = function() {
      console.log('Getting Location again!');
      $cordovaGeolocation
      .getCurrentPosition(posOptions)
      .then(function (position) {
        $scope.map = { center: { latitude: position.coords.latitude, longitude: position.coords.longitude }, zoom: 14 };
        console.log(position);
        $scope.formData.location = 'http://www.google.com/maps/place/' + position.coords.latitude + ',' + position.coords.longitude;
        $scope.locationAcquired = true;
      }, function(err) {
        $cordovaDialogs.alert('There was an error determining your location.', 'Error');
        $scope.locationAcquired = false;
      });
    }

    // $scope.addImages = function() {
    //   if ($scope.images.length == 5) {
    //     $cordovaDialogs.alert('You have reached the maximum allowed images.', 'Maximum Images Reached').then(function(){
    //       return;
    //     });
    //   }
    //   document.addEventListener("deviceready", function () {
    //     $cordovaActionSheet.show(actionSheetOptions)
    //       .then(function(btnIndex){
    //         if (btnIndex == 1){
    //           cameraOptions.sourceType = Camera.PictureSourceType.CAMERA;
    //         }
    //         else if (btnIndex == 2) {
    //           cameraOptions.sourceType = Camera.PictureSourceType.PHOTOLIBRARY;
    //         }
    //         else {
    //           return;
    //         }
    //
    //         $cordovaCamera.getPicture(cameraOptions).then(function(imageData) {
    //             $scope.images.push(imageData);
    //           }, function(err) {
    //             console.log(err);
    //           });
    //         }, false);
    //
    //       });
    // }
    //
    // $scope.removeImage = function(index) {
    //   $scope.images.splice(index, 1);
    // }

    $scope.formData = {};
    $scope.errors = {};



    $scope.submit = function() {

      //Create an object compatible with our Gravity Form
      var submissionObj = {
        input_values: {
          input_1: $scope.formData.message,
          input_2: $scope.formData.designation,
          input_3: $scope.formData.otherDesignation,
          input_4: $scope.formData.school,
          input_5: $scope.formData.name,
          input_6: $scope.formData.email,
          input_7: $scope.formData.phone,
          input_8: $scope.formData.location
        }
      };

      $http({
        method: 'POST',
        url: 'http://danverspublicschools.org/gravityformsapi/forms/16/submissions',
        data: submissionObj,
      }).then(function(data){
        console.log(data);
        $scope.processing = false;
        var response = data.data.response;
        if (!response.is_valid) {
          $scope.message = 'There were errors with your submission. Please review and try again.';
        }
        else {
          $scope.message = 'We have received your message and will get back to you as soon as possible.';
          $scope.formData = {};
        }
      }, function(err){
        $scope.message = 'There was an error processing your submission. Please check your network connection and try again.';
      });
    }
  }
]);
