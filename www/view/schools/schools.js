'use strict';

angular.module('danvers')

.config(['$stateProvider', function($stateProvider){
  $stateProvider.state('tab.schools', {
    url: '/schools',
    views: {
      'tab-schools': {
        templateUrl: 'view/schools/schools.html',
        controller: 'SchoolsCtrl'
      }
    }
  })
}])

.controller('SchoolsCtrl', [
  '$scope',
  'Schools',
  function($scope, Schools) {
    $scope.schools = Schools.all();
  }
]);
