'use strict';

angular.module('danvers')

.config(['$stateProvider', function($stateProvider){
  $stateProvider.state('tab.dash', {
    url: '/dash',
    views: {
      'tab-dash': {
        templateUrl: 'view/dashboard/dashboard.html',
        controller: 'DashCtrl'
      }
    }
  })
}])

.controller('DashCtrl', [
  '$scope',
  '$q',
  'Facebook',
  'Twitter',
  '$cordovaStatusbar',
  '$ionicPlatform',
  '$ionicModal',
  '$ionicLoading',
  '$state',
  '$cordovaAppAvailability',
  '$localstorage',
  '$ionicHistory',
  function($scope, $q, Facebook, Twitter, $cordovaStatusbar, $ionicPlatform, $ionicModal, $ionicLoading, $state, $cordovaAppAvailability, $localstorage, $ionicHistory) {

    $ionicLoading.show({
      template: 'Loading...'
    });

    function compareDates(a, b) {
      return b.created_at - a.created_at;
    }
    var offset = 0; //set the initial offset
    var maxId = 0;

    function getSocialPosts(){
      // Wait until the promises are resolved then populate the social results
      $q.all([Facebook.getPosts(), Twitter.getTweets()]).then(function(data){

        //create a master array
        var allPosts = [];
        var facebookPosts = normailizeFacebookPosts(data[0]);
        var twitterPosts = normailizeTwitterPosts(data[1]);

        allPosts = facebookPosts.concat(twitterPosts);

        allPosts.sort(compareDates);
        $scope.allPosts = allPosts;
        $ionicLoading.hide();
      }, function(error){
        $scope.error = 'There was an error loading the social stream. Please check your network connection.';
        $ionicLoading.hide();
      });
    }

    function normailizeTwitterPosts(data) {
      var twitter = data.data;
      var out = [];
      for (var i = 0; i < twitter.length; i++) {
        var date = moment(twitter[i].created_at, "ddd MMM DD HH:mm:ss +Z YYYY");
        var post = {
          network: 'twitter',
          profile: {
            name: '@' + twitter[i].user.screen_name,
            link: 'twitter://user?screen_name=' + twitter[i].user.screen_name,
          },
          message: twitter[i].text,
          url: 'http://twitter.com/' + twitter[i].user.screen_name + '/status/' + twitter[i].id_str,
          created_at: date,
        };
        if (i == (twitter.length - 1)){
          maxId = twitter[i].id;
        }
        out.push(post);
      }
      return out;
    }

    function normailizeFacebookPosts(data) {
      var facebook = data.data.data;
      var out = [];
      for (var i = 0; i < facebook.length; i++) {
        var date = moment(facebook[i].created_time, "YYYY-MM-DDTHH:mm:ss+Z");

        //We only get the shitty version photo from the API so we have to massage it to get the one we want
        var picture = facebook[i].picture;
        if (picture) {
            if (facebook[i].type === 'photo') {
                if (picture.indexOf('_s') !== -1) {
                    console.log('CONVERTING');
                    picture = picture.replace(/_s/, '_o');
                } else if (facebook[i].object_id) {
                    picture = 'https://graph.facebook.com/' + facebook[i].object_id + '/picture?width=9999&height=9999';
                }
            } else {
                var qps = facebook[i].picture.split('&');
                for (var j = 0; j < qps.length; j++) {
                    var qp = qps[j];
                    var matches = qp.match(/(url=|src=)/gi);
                    if (matches && matches.length > 0) picture = decodeURIComponent(qp.split(matches[0])[1]);
                }
            }
        }

        var post = {
          network: 'facebook',
          profile: {
            name: 'Danvers Public Schools',
            link: 'https://www.facebook.com/Danvers-Public-Schools-281720751861771/'
          },
          title: facebook[i].name ? facebook[i].name : '',
          picture: picture,
          message: facebook[i].message ? facebook[i].message : facebook[i].description,
          url: facebook[i].link,
          created_at: date
        };
        out.push(post);
      }
      return out;
    }

    //load the initial posts on dash load
    getSocialPosts();


    $scope.openProfile = function(profileLink, network) {
      //need to add logic here for android
      var scheme;
      if (network == 'facebook') {
        scheme = 'fb://';
      }
      else if (network == 'twitter') {
        scheme = 'twitter://'
      }
      $cordovaAppAvailability.check(scheme)
        .then(function(){
          window.open('fb://page/281720751861771', '_system');
        }, function(){
          window.open(profileLink, '_blank');
        });

    }

    $scope.loadMore = function() {
      //This will only work if we have access to native apis
      if (!window.cordova) return;

      //Facebook takes an offset so we can set that easily by page
      offset++;
      Facebook.setOffset(offset);
      //Twitter requires a max id
      Twitter.setMaxId(maxId);

      $q.all([Facebook.getPosts(), Twitter.getTweets()]).then(function(data){
        var facebook = normailizeFacebookPosts(data[0]);
        var twitter = normailizeTwitterPosts(data[1]);
        var newPosts = facebook.concat(twitter);
        newPosts.sort(compareDates);
        $scope.allPosts = $scope.allPosts.concat(newPosts);
        //var allPosts = newPosts.concat($scope.allPosts);
        //$scope.allPosts = allPosts.sort(compareDates);
        $scope.$broadcast('scroll.infiniteScrollComplete');
      });

    }

    $scope.openStaffDirectory = function() {
      $state.go('tab.more').then(function(){
        $state.go('tab.staff');
      });
    }

    $scope.openSchools = function() {
      $state.go('tab.schools');
    }

    $scope.share = function(post) {
      window.plugins.socialsharing.share(post.excerpt, post.title, null, post.link);
    }

}]);
