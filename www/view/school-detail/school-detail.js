'use strict';

angular.module('danvers')

.config(['$stateProvider', function($stateProvider){
  $stateProvider.state('tab.school-detail', {
    url: '/schools/:schoolId',
    views: {
      'tab-schools': {
        templateUrl: 'view/school-detail/school-detail.html',
        controller: 'SchoolDetailCtrl'
      }
    }
  })
}])

.controller('SchoolDetailCtrl', [
  '$scope',
  '$ionicPlatform',
  '$stateParams',
  'Schools',
  '$cordovaContacts',
  '$cordovaDialogs',
  '$cordovaActionSheet',
  function($scope, $ionicPlatform, $stateParams, Schools, $cordovaContacts, $cordovaDialogs, $cordovaActionSheet) {
    $scope.school = Schools.get($stateParams.schoolId);
    $scope.school.id = $stateParams.schoolId;

    //The following allows us to "preload" the handbook into the InAppBrowser so
    //it pops up right away when it's clicked on
    var url = '';
    if (ionic.Platform.isIOS()) {
      url = $scope.school.handbook;
    }
    else {
      url = 'http://docs.google.com/viewer?url=' + $scope.school.handbook;
    }

    $scope.openHandbook = function() {
      window.open(url, '_blank', 'EnableViewPortScale=yes');
    }

    $scope.addToContacts = function(school) {
      document.addEventListener("deviceready", function(){
        var contact = navigator.contacts.create({"displayName": "Test User"});
        contact.save(function(){
          $cordovaDialogs.alert('Contact saved successfully.', 'Success!');
        }, function(contactError){
          $cordovaDialogs.alert('There was an error saving your contact.', 'Error ' + contactError.code);
        });
      }, false);
    }

    $scope.launchDirections = function(school) {
      launchnavigator.navigate(school.location.address);
    }
  }
]);
