'use strict';

angular.module('danvers')

.config(['$stateProvider', function($stateProvider){
  $stateProvider.state('tab', {
    url: '/tab',
    abstract: true,
    templateUrl: 'view/tabs/tabs.html'
  })
}])

.controller('NavCtrl', [
  '$scope',
  '$state',
  function($scope, $state) {

  }
]);
