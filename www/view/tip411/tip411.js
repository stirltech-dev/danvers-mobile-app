'use strict';

angular.module('danvers')

.config(['$stateProvider', function($stateProvider){
  $stateProvider.state('tab.tip411', {
    url: '/tip411',
    views: {
      'tab-more': {
        templateUrl: 'view/tip411/tip411.html',
        controller: 'Tip411Ctrl'
      }
    }
  })
}])

.controller('Tip411Ctrl', [
  '$scope',
  '$cordovaSms',
  '$cordovaContacts',
  '$cordovaDialogs',
  '$ionicLoading',
  function($scope, $cordovaSms, $cordovaContacts, $cordovaDialogs, $ionicLoading){

    document.addEventListener("deviceready", function () {

      $cordovaDialogs.alert('If you are in danger or this is an emergency, please dial 911.', 'Important');

      $scope.sendTip = function() {


        //We add this in because it can take a second to load up
        // $ionicLoading.show({
        //   template: 'Loading...'
        // });

        var number = '847411',
            message = 'DanversSRO \n---\n',
            options = {
              replaceLineBreaks: true, // true to replace \n by a new line, false by default
              android: {
                  intent: 'INTENT'  // send SMS with the native android SMS messaging
              }
          };
        window.open('sms:+' + number + '&body=' + encodeURIComponent(message), '_system');
      }

      // sms.send(number, message, options,
      //    function() {
      //      $cordovaDialogs.alert('Thanks for helping to keep our community safe!', 'Thanks!');
      //     //  $ionicLoading.hide();
      //    }, function(error) {
      //     //  $ionicLoading.hide();
      //      console.warn(error);
      //    });
      //  }

      //   $cordovaSms
      //    .send(number, message, options)
      //    .then(function() {
      //      $cordovaDialogs.alert('Thanks for helping to keep our community safe!', 'Thanks!');
      //      $ionicLoading.hide();
      //    }, function(error) {
      //      $ionicLoading.hide();
      //    });
      //  }

       $scope.addToAddressBook = function() {
         var contact = {
           "displayName": "Danvers Police Tip Line",
           "phoneNumbers": [
              {
                  "value": "847411",
                  "type": "mobile"
              }
          ],
        };
         var tiplineContact = navigator.contacts.create(contact);
         $cordovaContacts.save(tiplineContact).then(function(result) {
           $cordovaDialogs.alert('Contact saved successfully as "' + contact.displayName + '"', 'Success!');
         }, function(err) {
           $cordovaDialogs.alert('There was an error saving your contact\n\n' + err, 'Error');
         });
       }
    });
  }
]);
