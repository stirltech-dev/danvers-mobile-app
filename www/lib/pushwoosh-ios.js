function initPushwooshiOS() {
    var pushwoosh = cordova.require("pushwoosh-cordova-plugin.PushNotification");
    //set push notification callback before we initialize the plugin
    document.addEventListener('push-notification', function(event) {
        //get the notification payload
        var notification = event.notification;

        //display alert to the user for example
        alert(notification.aps.alert);

        //clear the app badge
        pushwoosh.setApplicationIconBadgeNumber(0);
    });

    //initialize the plugin
    pushwoosh.onDeviceReady({pw_appid:"FD807-89D5C"});

    //register for pushes
    pushwoosh.registerDevice(
        function(status) {
            var deviceToken = status['deviceToken'];
            console.warn('registerDevice: ' + deviceToken);
        },
        function(status) {
            console.warn('failed to register : ' + JSON.stringify(status));
            alert(JSON.stringify(['failed to register ', status]));
        }
    );

    //reset badges on app start
    pushwoosh.setApplicationIconBadgeNumber(0);
}
